import React, { Component } from 'react';

class Card extends Component {
  render() {
    return (
      <div className='card'>
        <div className='card-thumbnail'>
          <img src="" alt="" />
        </div>
        <div className='card-title'>
          <a href="">title</a>
        </div>
        <div className='card-description'>
          <p>nome canal</p>
          <p>views</p>
          <p>tempo</p>
        </div>
      </div>
    )
  }
}
export default Card;
import React, { Component } from 'react';
import { connect } from 'react-redux';

class Sidebar extends Component {
  constructor(props) {
    super(props)
    console.info(props)
  }
  render() {
    return (
      <div className={'sidebar ' + (this.props.toggleMenu ? 'sidebar-hidden' : '')}>
        <ul>
          <li>teste</li>
          <li>teste</li>
        </ul>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  toggleMenu: state.Sidebar
})

export default connect(mapStateToProps)(Sidebar);
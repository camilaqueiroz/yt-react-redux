import React, { Component } from 'react';

class Search extends Component {
  render() {
    return (
      <form className='search'>
        <input className='search-input' type='text' placeholder='o que você busca?' />
        <button className='button button-search' type='submit'><i className="fas fa-search"></i></button>
      </form>
    )
  }
}

export default Search;
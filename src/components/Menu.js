import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Sidebar from '../actions/Sidebar';

import Search from './Search';

class Menu extends Component {
  constructor(props) {
    super(props)
  }
  toggleSidebar = () => {
    this.props.toggleSidebar()
  }
  render() {
    return (
      <nav className='navbar'>
        <div className='navbar-right-buttons'>
          <i className="fas fa-bars toggleSidebar" onClick={this.toggleSidebar}></i>
          <Link className='brand' to="/"><img src="https://bit.ly/2KcmfDb" alt="Logotipo" /></Link>
        </div>
        <Search></Search>
        <ul className='menu-inline-list'>
          <li><Link to="/"><i className="fas fa-bars"></i></Link></li>
          <li><Link to="/list"><i className="fas fa-bars"></i></Link></li>
          <li><Link to="/list"><i className="fas fa-bars"></i></Link></li>
          <li><Link to="/list"><i className="fas fa-bars"></i></Link></li>
        </ul>
      </nav>
    )
  }
}

const mapDispatchToProps = dispatch =>
  bindActionCreators(Sidebar, dispatch)

export default connect(null, mapDispatchToProps)(Menu);
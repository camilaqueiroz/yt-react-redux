import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';

import Menu from './components/Menu';
import Sidebar from './components/Sidebar';
import Home from './pages/Home';
import List from './pages/List';

class App extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    return (
      <div>
        <Router>
          <Menu></Menu>
          <div className='wrapper'>
            <Sidebar></Sidebar>
            <main className={'content ' + (this.props.toggleMenu ? 'no-margin-content' : '')}>
              <Switch>
                <Route exact component={Home} path='/' />
                <Route exact component={List} path='/list' />
              </Switch>
            </main>
          </div>
        </Router>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  toggleMenu: state.Sidebar
})

export default connect(mapStateToProps)(App);
